const q = new URLSearchParams(window.location.search).get('q');

if (q) {
    window.location.href = `http://google.com/search?q=${q}`;
}
